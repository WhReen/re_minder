package id.ac.ui.cs.reminderbot.controller;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.command.help.HelpBucketList;
import id.ac.ui.cs.reminderbot.repository.CommandRepository;
import id.ac.ui.cs.reminderbot.repository.CommandRepositoryImpl;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FiturCommandImplTest {

    @Mock
    private CommandRepository commandRepository;

    @Mock
    private Message message;

    @Mock
    private Command command;

    @Mock
    private net.dv8tion.jda.api.entities.User DiscordUser;

    @Mock
    private net.dv8tion.jda.api.entities.Guild DiscordGuild;

    @Mock
    private net.dv8tion.jda.api.entities.MessageChannel DiscordChannel;

    @Mock
    private MessageAction messageAction;
    
    @InjectMocks
    private FiturCommandImpl underTest;

    Class<?> fiturCommandImplClass;
    private FiturCommandImpl fiturCommandImpl;
    private CommandRepositoryImpl commandRepositoryImpl;

    @BeforeEach
    public void setUp() throws ClassNotFoundException {
        fiturCommandImplClass = Class.forName("id.ac.ui.cs.reminderbot.controller.FiturCommandImpl");
        commandRepositoryImpl = new CommandRepositoryImpl();
        fiturCommandImpl = new FiturCommandImpl(commandRepositoryImpl);
    }

    @Test
    public void testFiturCommandImplIsClass(){
        int classModifiers = fiturCommandImplClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(fiturCommandImpl instanceof FiturCommand);
    }

    @Test
    public void testFiturCommandImplHasFormCommandMethod() throws NoSuchMethodException {
        Method getInstance = fiturCommandImplClass.getDeclaredMethod("formCommand", String[].class);
        int methodModifiers = getInstance.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getInstance.getParameterCount());
    }

    @Test
    public void testFiturCommandImplWithOneWordInput(){
        String[] input = "!help".split(" ");
        assertEquals("help", fiturCommandImpl.formCommand(input));
    }

    @Test
    public void testFiturCommandImplWithTwoWordInput(){
        String[] input = "!help bucketlist".split(" ");
        assertEquals("help bucketlist", fiturCommandImpl.formCommand(input));
    }

    @Test
    public void testFiturCommandImplGetOutputMessageSucceeded(){
        String[] input = "!help bucketlist".split(" ");
        String content = "help bucketlist";

        when(commandRepository.getCommand(content)).thenReturn(command);
        underTest.outputMessage(message, input);

        verify(commandRepository, times(1)).getCommand(content);
        verify(command, times(1)).getOutputMessage(message, input);
    }

    @Test
    public void testFiturCommandImplGetOutputMessageFailed(){
        String[] input = "!help bucketlist".split(" ");
        String content = "help bucketlist";

        when(commandRepository.getCommand(content)).thenReturn(command);

        when(message.getChannel()).thenReturn(DiscordChannel);
        doThrow(IllegalStateException.class).when(command).getOutputMessage(any(Message.class), any(String[].class));
        when(message.getChannel()).thenReturn(DiscordChannel);
        when(DiscordChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);
        underTest.outputMessage(message, input);

        verify(commandRepository, times(1)).getCommand(content);
        verify(command, times(1)).getOutputMessage(message, input);
        verify(message, times(1)).getChannel();
        verify(DiscordChannel, times(1)).sendMessage(any(MessageEmbed.class));
        verify(messageAction, times(1)).queue();
    }
}

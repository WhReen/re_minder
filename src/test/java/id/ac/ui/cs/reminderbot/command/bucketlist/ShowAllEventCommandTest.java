package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShowAllEventCommandTest {
    @Mock
    private BucketlistService bucketlistService;

    @Mock
    private Message message;

    @Mock
    private net.dv8tion.jda.api.entities.User DiscordUser;

    @Mock
    private net.dv8tion.jda.api.entities.Guild DiscordGuild;

    @Mock
    private net.dv8tion.jda.api.entities.MessageChannel DiscordChannel;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private ShowAllEventCommand underTest;


    private Event bucketlist;
    private String guildID;
    private ArrayList<Event> bucketlists;

    @BeforeEach
    void setup() {
        guildID = "851945694930665492";
        bucketlist = new Event("nameBucketlist", guildID);
        bucketlists = new ArrayList<>();
        bucketlists.add(bucketlist);
    }

    @Test
    public void testShowAllEventCommandHasGetOutputCommand(){
        String[] input = {"!bucketlist", "showAllEvent"};
        when(message.getGuild()).thenReturn(DiscordGuild);
        when(DiscordGuild.getId()).thenReturn(guildID);
        when(bucketlistService.showAllEvent(guildID)).thenReturn(bucketlists);
        when(message.getChannel()).thenReturn(DiscordChannel);
        when(DiscordChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);
        underTest.getOutputMessage(message, input);
        verify(bucketlistService, times(1)).showAllEvent(guildID);
    }
}

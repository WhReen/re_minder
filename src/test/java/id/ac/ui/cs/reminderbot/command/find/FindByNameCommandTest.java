package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindByNameCommandTest {
    @Mock
    BucketlistService bucketlistService;

    @Mock
    TugasService tugasService;

    @Mock
    KegiatanService kegiatanService;

    @Mock
    MessagingService messagingService;

    @Spy
    @InjectMocks
    FindByNameCommand subject;

    @Mock
    Message dummyMessage;

    @Captor
    ArgumentCaptor<Color> colorArgumentCaptor;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testNull() {
        when(kegiatanService.findEventsByName(anyString())).thenReturn(null);
        when(tugasService.findEventsByName(anyString())).thenReturn(null);
        when(bucketlistService.findEventsByName(anyString())).thenReturn(null);

        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());

        subject.queryTemplate(dummyMessage, new String[] {"!find", "byname", "sneed"}, "name");
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).getNullListMessage();
        assertEquals(Color.RED, colorArgumentCaptor.getValue());
    }

    @Test
    public void testEmpty() {
        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());
        subject.queryTemplate(dummyMessage, new String[] {"!find", "byname", "sneed"}, "name");
        verify(messagingService, atLeastOnce()).sendRightNow(any(), any());
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).getEmptyListMessage(anyString(), anyString());
        assertEquals(Color.RED, colorArgumentCaptor.getValue());
    }

    @Test
    public void testWithSomething() throws ParseException {
        java.util.List<Event> bucketlistListMock = new ArrayList<>(
                List.of(new Event())
        );
        java.util.List<Tugas> tugasListMock = new ArrayList<>(
                List.of(new Tugas("testTugas",
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00"),
                        "851945694930665492"))
        );
        java.util.List<Kegiatan> kegiatanListMock = new ArrayList<>(
                List.of(new Kegiatan("foo",
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00")
                                .toInstant().atOffset(ZoneOffset.ofHours(7)),
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00")
                        .toInstant().atOffset(ZoneOffset.ofHours(7)),
                        "bar", true))
        );
        when(kegiatanService.findEventsByName("sneed")).thenReturn(kegiatanListMock);
        when(tugasService.findEventsByName("sneed")).thenReturn(tugasListMock);
        when(bucketlistService.findEventsByName("sneed")).thenReturn(bucketlistListMock);

        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());
        subject.queryTemplate(dummyMessage, new String[] {"!find", "byname", "sneed"}, "name");
        verify(messagingService, atLeastOnce()).sendRightNow(any(), any());
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).appendListsToEmbedBuilder(any(), any(), any(), any());
        assertEquals(Color.YELLOW, colorArgumentCaptor.getValue());

    }
}

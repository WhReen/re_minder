package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindByDeadlineCommandTest {
    @Mock
    TugasService tugasService;

    @Mock
    KegiatanService kegiatanService;

    @Mock
    MessagingService messagingService;

    @Spy
    @InjectMocks
    FindByDeadlineCommand subject;

    @Mock
    Message dummyMessage;

    @Captor
    ArgumentCaptor<Color> colorArgumentCaptor;

    OffsetDateTime mockOffsetDateTime;
    Date mockDate;

    @BeforeEach
    public void setUp() throws ParseException {
        mockDate = new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00");
        mockOffsetDateTime = mockDate.toInstant().atOffset(ZoneOffset.ofHours(7));

    }

    @Test
    public void testNull() {
        when(kegiatanService.findEventsByDeadline(any(OffsetDateTime.class))).thenReturn(null);
        when(tugasService.findEventsByDeadline(any(Date.class))).thenReturn(null);

        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());

        subject.queryTemplate(dummyMessage, new String[] {"!find", "bydate", "31-09-3000_21:00"}, "date");
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).getNullListMessage();
        assertEquals(Color.RED, colorArgumentCaptor.getValue());
    }

    @Test
    public void testEmpty() {
        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());
        subject.queryTemplate(dummyMessage, new String[] {"!find", "bydate", "31-09-3000_21:00"}, "date");
        verify(messagingService, atLeastOnce()).sendRightNow(any(), any());
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).getEmptyListMessage(anyString(), anyString());
        assertEquals(Color.RED, colorArgumentCaptor.getValue());
    }

    @Test
    public void testWithSomething() throws ParseException {
        java.util.List<Tugas> tugasListMock = new ArrayList<>(
                                List.of(new Tugas("testTugas",
                                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00"),
                                        "851945694930665492"))
                        );
        java.util.List<Kegiatan> kegiatanListMock = new ArrayList<>(
                List.of(new Kegiatan("foo",
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00")
                                .toInstant().atOffset(ZoneOffset.ofHours(7)),
                        new SimpleDateFormat("dd-MM-yyyy_HH:mm").parse("31-09-3000_21:00")
                                .toInstant().atOffset(ZoneOffset.ofHours(7)),
                        "bar", true))
        );
        when(kegiatanService.findEventsByDeadline(mockOffsetDateTime)).thenReturn(kegiatanListMock);
        when(tugasService.findEventsByDeadline(mockDate)).thenReturn(tugasListMock);

        when(dummyMessage.getTextChannel()).thenReturn(null);
        doNothing().when(messagingService).sendRightNow(any(), any());
        subject.queryTemplate(dummyMessage, new String[] {"!find", "bydate", "31-09-3000_21:00"}, "date");
        verify(messagingService, atLeastOnce()).sendRightNow(any(), any());
        verify(subject).buildMessage(anyString(), colorArgumentCaptor.capture(), anyString());
        verify(subject, times(1)).appendListsToEmbedBuilder(any(), any(), any(), any());
        assertEquals(Color.YELLOW, colorArgumentCaptor.getValue());

    }
}

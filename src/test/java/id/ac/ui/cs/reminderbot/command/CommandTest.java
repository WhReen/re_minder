package id.ac.ui.cs.reminderbot.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandTest {
    Class<?> commandClass;
    Class<?> message;

    @BeforeEach
    public  void setUp() throws Exception{
        commandClass = Class.forName("id.ac.ui.cs.reminderbot.command.Command");
        message = Class.forName("net.dv8tion.jda.api.entities.Message");
    }

    @Test
    public void testCommandIsInterface(){
        int classModifiers = commandClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testCommandGetOutputMessageMethod() throws Exception {
        Class<?> stringInput = String[].class;
        Method getOutputMessage = commandClass.getDeclaredMethod("getOutputMessage", message, stringInput);
        int methodModifiers = getOutputMessage.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, getOutputMessage.getParameterCount());
    }
}

package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.repository.TugasRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TugasServiceImplTest {
    @Mock
    TugasRepository tugasRepository;

    @InjectMocks
    TugasServiceImpl tugasService;

    private Tugas tugas;
    private String guildID;
    private Date deadline;
    private String authorID;

    @BeforeEach
    public void setUp() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        deadline = dateFormat.parse("31-09-3000_21:00");
        tugas = new Tugas("testTugas", deadline, guildID );
    }

    @Test
    public void testTugasServiceHasAddTugasMethod(){
        when(tugasRepository.save(tugas)).thenReturn(tugas);
        Tugas tugasToAdd = tugasService.addTugas(tugas);
        assertEquals(tugas, tugasToAdd);
    }

    @Test
    public void testTugasServiceHasShowTugasMethod(){
        when(tugasRepository.findById(tugas.getId())).thenReturn(tugas);
        Tugas tugasToAdd = tugasService.showTugas(tugas.getId());
        assertEquals(tugas, tugasToAdd);
    }

    @Test
    public void testTugasServiceHasCheckTugasMethod(){
        when(tugasRepository.findById(tugas.getId())).thenReturn(tugas);
        Tugas tugasChecked  = tugasService.checkTugasById(tugas.getId());
        assertTrue(tugasChecked.isChecked());
    }

    @Test
    public void testTugasServiceHasShowAllMethod(){
        ArrayList<Tugas> listOfTasks = new ArrayList();
        listOfTasks.add(tugas);
        when(tugasRepository.findAll()).thenReturn(listOfTasks);
        Iterable<Tugas> listofTugas = tugasService.showAllTugas(guildID);
        assertEquals(listOfTasks, listofTugas);
    }

    @Test
    public void testTugasServiceHasDeleteTugasMethod(){
        when(tugasRepository.save(tugas)).thenReturn(tugas);
        when(tugasRepository.findById(tugas.getId())).thenReturn(tugas);
        tugasService.addTugas(tugas);
        assertEquals(tugas, tugasService.showTugas(tugas.getId()));

        tugasService.deleteTugas(tugas.getId());
        assertEquals(tugas, tugasService.showTugas(tugas.getId()));
    }

    @Test
    public void testTugasDeleteNull(){
        assertEquals(null, tugasService.deleteTugas(10));
    }

    @Test
    public void testFindByName(){
        assertTrue(tugasService.findEventsByName(tugas.getNamaTugas()).isEmpty());
    }

    @Test
    public void testFindByDeadline(){
        assertTrue(tugasService.findEventsByDeadline(tugas.getDeadline()).isEmpty());
    }
}

package id.ac.ui.cs.reminderbot.service;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MessagingServiceImplTest {
    @Mock
    TextChannel textChannelMock;

    @Mock
    EmbedBuilder embedBuilderMock;

    @Spy
    MessagingServiceImpl subject;

    @Captor
    ArgumentCaptor<MessageEmbed> messageEmbedArgumentCaptor;

    @Mock
    MessageEmbed messageEmbedMock;

    @Mock
    MessageAction messageActionMock;
    @Test
    public void testSendRightNow() {
        when(textChannelMock.sendMessage(any(MessageEmbed.class))).thenReturn(messageActionMock);
        doNothing().when(messageActionMock).queue();
        when(embedBuilderMock.build()).thenReturn(messageEmbedMock);
        subject.sendRightNow(embedBuilderMock, textChannelMock);
        verify(textChannelMock, times(1)).sendMessage(messageEmbedArgumentCaptor.capture());
        assertEquals(messageEmbedMock, messageEmbedArgumentCaptor.getValue());
    }
}

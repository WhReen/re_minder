package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.model.Tugas;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MultiTypeEventListTest {

    MultiTypeEventList subject;
    List<Event> bucketlistList;
    List<Tugas> tugasList;
    List<Kegiatan> kegiatanList;
    @BeforeEach
    public void setUp() throws Exception {
    }

    @Test
    public void testBasicInstantiation() {
        bucketlistList = new ArrayList<>();
        tugasList = new ArrayList<>();
        kegiatanList = new ArrayList<>();
        subject = new MultiTypeEventList(bucketlistList, tugasList, kegiatanList);
        assertEquals(bucketlistList, subject.getBucketlistList());
        assertEquals(tugasList, subject.getTugasList());
        assertEquals(kegiatanList, subject.getKegiatanList());
    }
}

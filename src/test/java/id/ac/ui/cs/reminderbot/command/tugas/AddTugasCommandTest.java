package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AddTugasCommandTest {
    @Mock
    private TugasService tugasService;

    @Mock
    private Message message;

    @Mock
    private User discordUser;

    @Mock
    private Guild discordGuild;

    @Mock
    private MessageChannel messageChannel;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private AddTugasCommand addTugasCommand;

    private Tugas tugas;
    private String guildID;
    private Date deadline;
    private String authorID;

    @BeforeEach
    void setup() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        deadline = dateFormat.parse("31-09-3000_21:00");
        tugas = new Tugas("testTugas", deadline, guildID );
    }

    @Test
    public void testAddTugasCommandGetOutputMessage(){
/*        String[] input = {"!tugas", "add", "testTugas", "31-09-2030_21:00"};
        when(message.getAuthor())
                .thenReturn(discordUser);
        when(discordUser.getId())
                .thenReturn(authorID);
        when(message.getGuild())
                .thenReturn(discordGuild);
        when(discordGuild.getId())
                .thenReturn(guildID);
        when(tugasService.addTugas(any(Tugas.class)))
                .thenReturn(tugas);
        when(message.reply(any(MessageEmbed.class)))
                .thenReturn(messageAction);
        when(message.getChannel())
                .thenReturn(messageChannel);
        lenient().when(messageChannel
                .sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);
        addTugasCommand.getOutputMessage(message, input);*/
    }
}

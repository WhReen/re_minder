package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DeleteTodoCommandTest {
    @Mock
    private BucketlistService bucketlistService;

    @Mock
    private Message message;

    @Mock
    private net.dv8tion.jda.api.entities.MessageChannel DiscordChannel;

    @Mock
    private net.dv8tion.jda.api.entities.User DiscordUser;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private DeleteTodoCommand underTest;

    private Event bucketlist;
    private Todo todo;
    private String guildID;
    private String authorID;

    @BeforeEach
    void setup() {
        authorID = "692251856218619977";
        guildID = "851945694930665492";
        bucketlist = new Event("nameBucketlist", guildID);
        todo = new Todo("nameTodo");
        bucketlist.getTodoitemlist().add(todo);
    }

    @Test
    public void testDeleteEventCommandHasGetOutputCommand(){
        String bucketlistID = Integer.toString(bucketlist.getId());
        String todoID = Integer.toString(todo.getId());
        String[] input = {"!bucketlist", "deleteTodo", bucketlistID, todoID};
        when(bucketlistService.deleteTodo(any(Integer.class))).thenReturn(todo);
        when(message.getChannel()).thenReturn(DiscordChannel);
        when(DiscordChannel.sendMessage(any(MessageEmbed.class))).thenReturn(messageAction);
        underTest.getOutputMessage(message, input);
        verify(bucketlistService, times(1)).deleteTodo(todo.getId());
    }
}

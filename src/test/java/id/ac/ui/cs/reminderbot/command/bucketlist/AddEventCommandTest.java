package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AddEventCommandTest {
    @Mock
    private BucketlistService bucketlistService;

    @Mock
    private Message message;

    @Mock
    private net.dv8tion.jda.api.entities.User DiscordUser;

    @Mock
    private net.dv8tion.jda.api.entities.Guild DiscordGuild;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private AddEventCommand underTest;

    private Event bucketlist;
    private Todo todo;
    private String guildID;
    private String authorID;

    @BeforeEach
    void setup() {
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        bucketlist = new Event("nameBucketlist", guildID);
        todo = new Todo("nameTodo");
    }

    @Test
    public void testAddEventCommandHasGetOutputCommand(){
        String[] input = {"!bucketlist", "add", "nameBucketlist"};
        when(message.getAuthor()).thenReturn(DiscordUser);
        when(DiscordUser.getId()).thenReturn(authorID);
        when(message.getGuild()).thenReturn(DiscordGuild);
        when(DiscordGuild.getId()).thenReturn(guildID);
        when(bucketlistService.addEvent(any(Event.class))).thenReturn(bucketlist);
        when(message.reply(any(MessageEmbed.class))).thenReturn(messageAction);
        underTest.getOutputMessage(message, input);
    }
}

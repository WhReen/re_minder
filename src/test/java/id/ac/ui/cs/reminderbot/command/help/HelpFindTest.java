package id.ac.ui.cs.reminderbot.command.help;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HelpFindTest {
    @Spy
    HelpFind subject;

    @Mock
    Message messageMock;

    @Mock
    MessageChannel messageChannelMock;

    @Mock
    MessageAction messageActionMock;

    String[] inputMock = {"!help foo"};

    @Test
    public void testGetOutputMessage() {
        when(messageMock.getChannel()).thenReturn(messageChannelMock);
        when(messageChannelMock.sendMessage(any(MessageEmbed.class))).thenReturn(messageActionMock);
        doNothing().when(messageActionMock).queue();
        subject.getOutputMessage(messageMock, inputMock);
        verify(messageActionMock, atLeastOnce()).queue();
    }
}

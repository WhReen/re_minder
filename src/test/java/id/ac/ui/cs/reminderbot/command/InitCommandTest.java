package id.ac.ui.cs.reminderbot.command;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InitCommandTest {
    Class<?> initCommandClass;

    @BeforeEach
    public  void setUp() throws Exception{
        initCommandClass = Class.forName("id.ac.ui.cs.reminderbot.command.InitCommand");
    }

    @Test
    public void testInitCommandCommandIsClass(){
        int classModifiers = initCommandClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testHelpBucketListgetInitMethod() throws Exception {
        Method init = initCommandClass.getDeclaredMethod("init");
        int methodModifiers = init.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, init.getParameterCount());
    }
}

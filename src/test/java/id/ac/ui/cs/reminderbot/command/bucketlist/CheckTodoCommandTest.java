package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class CheckTodoCommandTest {
    @Mock
    private BucketlistService bucketlistService;

    @Mock
    private Message message;

    @Mock
    private net.dv8tion.jda.api.entities.User DiscordUser;

    @Mock
    private net.dv8tion.jda.api.entities.Guild DiscordGuild;

    @Mock
    private net.dv8tion.jda.api.entities.MessageChannel DiscordChannel;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private CheckTodoCommand underTest;

    private Event bucketlist;
    private Todo todo;
    private String guildID;
    private String authorID;

    @BeforeEach
    void setup() {
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        bucketlist = new Event("nameBucketlist", guildID);
        todo = new Todo("nameTodo");
        bucketlist.getTodoitemlist().add(todo);
    }

    @Test
    public void testCheckTodoCommandHasGetOutputCommand(){
        String bucketlistID = Integer.toString(bucketlist.getId());
        String todoID = Integer.toString(todo.getId());
        String[] input = {"!bucketlist", "checkTodo", bucketlistID, todoID};
        when(message.getAuthor()).thenReturn(DiscordUser);
        when(DiscordUser.getId()).thenReturn(authorID);
        when(bucketlistService.checkTodoById(any(Integer.class))).thenReturn(todo);
        when(message.reply(any(MessageEmbed.class))).thenReturn(messageAction);
        underTest.getOutputMessage(message, input);
        verify(bucketlistService, times(1)).checkTodoById(todo.getId());
    }
}

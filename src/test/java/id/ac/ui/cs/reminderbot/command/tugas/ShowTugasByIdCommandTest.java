package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShowTugasByIdCommandTest {
    @Mock
    private TugasService tugasService;

    @Mock
    private Message message;

    @Mock
    private User discordUser;

    @Mock
    private Guild discordGuild;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private ShowTugasByIdCommand showTugasByIdCommand;

    private Tugas tugas;
    private String guildID;
    private Date deadline;
    private String authorID;

    @BeforeEach
    void setup() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        deadline = dateFormat.parse("31-09-3000_21:00");
        tugas = new Tugas("testTugas", deadline, guildID );
    }

    @Test
    public void testShowTugasByIdShouldReturnMessage(){
        String id = Integer.toString(tugas.getId());
        String[] input = {"!tugas","show" ,id};
        when(tugasService.showTugas(tugas.getId())).thenReturn(tugas);
        when(message.reply(any(MessageEmbed.class))).thenReturn(messageAction);
        showTugasByIdCommand.getOutputMessage(message, input);
        verify(tugasService, times(1)).showTugas(tugas.getId());

    }
}

package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HelpMainTest {
    Class<?> helpMainClass;
    Class<?> message;

    @Spy
    HelpMain subject;

    @Mock
    Message messageMock;

    @Mock
    MessageChannel messageChannelMock;

    @Mock
    MessageAction messageActionMock;

    String[] inputMock = {"!help foo"};

    @BeforeEach
    public  void setUp() throws Exception{
        helpMainClass = Class.forName("id.ac.ui.cs.reminderbot.command.help.HelpMain");
        message = Class.forName("net.dv8tion.jda.api.entities.Message");
    }

    @Test
    public void testHelpMainIsClass(){
        int classModifiers = helpMainClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new HelpMain() instanceof Command);
    }

    @Test
    public void testHelpMaingetInstanceMethod() throws Exception {
        Method getInstance = helpMainClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
        assertEquals(0, getInstance.getParameterCount());
    }

    @Test
    public void testHelpBucketlistGetOutputMessageMethod() throws Exception {
        Class<?> stringInput = String[].class;
        Method getOutputMessage = helpMainClass.getDeclaredMethod("getOutputMessage", message, stringInput);
        int methodModifiers = getOutputMessage.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, getOutputMessage.getParameterCount());
    }

    @Test
    public void testGetOutputMessage() {
        when(messageMock.getChannel()).thenReturn(messageChannelMock);
        when(messageChannelMock.sendMessage(any(MessageEmbed.class))).thenReturn(messageActionMock);
        doNothing().when(messageActionMock).queue();
        subject.getOutputMessage(messageMock, inputMock);
        verify(messageActionMock, atLeastOnce()).queue();
    }
}

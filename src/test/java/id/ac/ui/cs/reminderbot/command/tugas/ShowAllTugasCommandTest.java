package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ShowAllTugasCommandTest {
    @Mock
    private TugasService tugasService;

    @Mock
    private Message message;

    @Mock
    private User discordUser;

    @Mock
    private Guild discordGuild;

    @Mock
    private MessageAction messageAction;

    @InjectMocks
    private ShowAllTugasCommand showAllTugasCommand;

    private Tugas tugas;
    private Tugas tugas2;
    private String guildID;
    private Date deadline;
    private String authorID;
    private ArrayList<Tugas> listTugas;


    @BeforeEach
    void setup() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        guildID = "851945694930665492";
        authorID = "692251856218619977";
        deadline = dateFormat.parse("31-09-3000_21:00");
        tugas = new Tugas("testTugas", deadline, guildID );
        tugas2 = new Tugas("testTugas2", deadline, guildID);
        listTugas = new ArrayList<>();
        tugasService.checkTugasById(1);
        listTugas.add(tugas);
        listTugas.add(tugas2);
    }

    @Test
    public void testShowAllTugas(){
        String[] input = {"!tugas", "showall"};
        when(message.getGuild()).thenReturn(discordGuild);
        lenient().when(discordGuild.getId()).thenReturn(guildID);
        when(tugasService.showAllTugas(guildID)).thenReturn(listTugas);
        when(message.reply(any(MessageEmbed.class))).thenReturn(messageAction);
        showAllTugasCommand.getOutputMessage(message, input);
        verify(tugasService, times(1)).showAllTugas(guildID);

    }
}

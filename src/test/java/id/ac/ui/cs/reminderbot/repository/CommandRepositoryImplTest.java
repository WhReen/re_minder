package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.command.Command;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandRepositoryImplTest {
    Class<?> commandRepositoryImplClass;

    @BeforeEach
    public  void setUp() throws Exception{
        commandRepositoryImplClass = Class.forName("id.ac.ui.cs.reminderbot.repository.CommandRepositoryImpl");
    }

    @Test
    public void testCommandRepositoryImplClassIsClass(){
        int classModifiers = commandRepositoryImplClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(new CommandRepositoryImpl() instanceof CommandRepository);
    }

    @Test
    public void testCommandRepositoryImplClasshasGetCommandMethod() throws Exception {
        Class<?> param = String.class;
        Method getCommand = commandRepositoryImplClass.getDeclaredMethod("getCommand", param);
        int methodModifiers = getCommand.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(1, getCommand.getParameterCount());
    }

    @Test
    public void testCommandRepositoryImplClasshasAddCommandMethod() throws Exception {
        Class<?> param1 = String.class;
        Class<?> param2 = Command.class;
        Method addCommand = commandRepositoryImplClass.getDeclaredMethod("addCommand", param1, param2);
        int methodModifiers = addCommand.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(2, addCommand.getParameterCount());
        assertEquals("void", addCommand.getGenericReturnType().getTypeName());
    }
}

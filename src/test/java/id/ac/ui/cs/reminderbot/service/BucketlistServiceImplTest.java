package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.repository.EventRepository;
import id.ac.ui.cs.reminderbot.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BucketlistServiceImplTest {
    @Mock
    private EventRepository eventRepository;
    @Mock
    private TodoRepository todoRepository;
    @InjectMocks
    private BucketlistServiceImpl underTest;

    private Event bucketlist;
    private Todo todo;
    private String guildID;

    @BeforeEach
    public void setUp(){
        guildID = "851945694930665492";
        bucketlist = new Event("nameBucketlist", guildID);
        todo = new Todo("nameTodo");
    }

    @Test
    public void testBucketlistServiceHasAddEventMethod(){
        when(eventRepository.save(bucketlist)).thenReturn(bucketlist);
        Event addedBucketlist = underTest.addEvent(bucketlist);
        assertEquals(bucketlist, addedBucketlist);
    }

    @Test
    public void testBucketlistServiceHasAddTodoMethod(){
        bucketlist.getTodoitemlist().add(todo);
        when(todoRepository.save(todo)).thenReturn(todo);
        when(eventRepository.findById(bucketlist.getId())).thenReturn(bucketlist);
        Event addedBucketlist = underTest.addTodo(bucketlist.getId(), todo);
        assertEquals(todo, addedBucketlist.getTodoitemlist().get(0));
    }

    @Test
    public void testBucketlistServiceHasShowEventMethod(){
        when(eventRepository.findById(bucketlist.getId())).thenReturn(bucketlist);
        Event addedBucketlist = underTest.showEvent(bucketlist.getId());
        assertEquals(bucketlist, addedBucketlist);
    }

    @Test
    public void testBucketlistServiceHasShowAllEventMethod(){
        ArrayList<Event> listOfTasks = new ArrayList();
        listOfTasks.add(bucketlist);
        when(eventRepository.findAll()).thenReturn(listOfTasks);
        Iterable<Event> listofBucketlist = underTest.showAllEvent(guildID);
        assertEquals(listOfTasks, listofBucketlist);
    }

    @Test
    public void testBucketlistServiceHasCheckTodoMethod(){
        bucketlist.getTodoitemlist().add(todo);
        when(todoRepository.findById(todo.getId())).thenReturn(todo);
        Todo checkedTodo  = underTest.checkTodoById(todo.getId());
        assertTrue(checkedTodo.isChecked());
    }

    @Test
    public void testBucketlistServiceHasDeleteTodoMethod(){
        underTest.deleteTodo(todo.getId());
        assertNull(underTest.showEvent(bucketlist.getId()));
    }

    @Test
    public void testBucketlistServiceHasDeleteEventMethod(){
        underTest.deleteEvent(bucketlist.getId());
        assertEquals(null, underTest.showEvent(bucketlist.getId()));
    }
}

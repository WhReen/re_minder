package id.ac.ui.cs.reminderbot;

import id.ac.ui.cs.reminderbot.controller.FiturCommand;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@ExtendWith(MockitoExtension.class)
public class InputEventListenerTest {
    @Mock
    FiturCommand fiturCommandMock;

    @Spy
    @InjectMocks
    InputEventListener subject;

    @Mock
    Message messageMock;

    @Mock
    GuildMessageReceivedEvent guildMessageReceivedEventMock;

    @Mock
    User botAuthorMock;

    String commandMock = "!help wilson";

    @Test
    public void testAuthorIsBot() {
        when(guildMessageReceivedEventMock.getMessage()).thenReturn(messageMock);
        when(messageMock.getAuthor()).thenReturn(botAuthorMock);
        when(botAuthorMock.isBot()).thenReturn(true);
        when(messageMock.getContentRaw()).thenReturn(commandMock);
        subject.onGuildMessageReceived(guildMessageReceivedEventMock);
        verify(fiturCommandMock, times(0)).outputMessage(any(), any());
    }

    @Test
    public void testRegularSend() {
        when(guildMessageReceivedEventMock.getMessage()).thenReturn(messageMock);
        when(messageMock.getAuthor()).thenReturn(botAuthorMock);
        when(botAuthorMock.isBot()).thenReturn(false);
        when(messageMock.getContentRaw()).thenReturn(commandMock);
        doNothing().when(fiturCommandMock).outputMessage(any(), any());
        setField(subject, "prefix", "!");
        subject.onGuildMessageReceived(guildMessageReceivedEventMock);
        verify(fiturCommandMock, times(1)).outputMessage(any(), any());
    }
    @Test
    public void testRegularSendBadPrefix() {
        when(guildMessageReceivedEventMock.getMessage()).thenReturn(messageMock);
        when(messageMock.getAuthor()).thenReturn(botAuthorMock);
        when(botAuthorMock.isBot()).thenReturn(false);
        when(messageMock.getContentRaw()).thenReturn(commandMock);
        setField(subject, "prefix", "#");
        subject.onGuildMessageReceived(guildMessageReceivedEventMock);
        verify(fiturCommandMock, times(0)).outputMessage(any(), any());
    }
}
package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.stereotype.Component;

@Component
public class FindByNameCommand extends FindByTemplate implements Command {
    public FindByNameCommand(BucketlistService bucketlistService, TugasService tugasService,
                             KegiatanService kegiatanService, MessagingService messagingService) {
        super(bucketlistService, tugasService, kegiatanService, messagingService);
    }

    protected MultiTypeEventList getMTEL(String arg) {
        return new MultiTypeEventList(
            bucketlistService.findEventsByName(arg),
            tugasService.findEventsByName(arg),
            kegiatanService.findEventsByName(arg)
        );
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        queryTemplate(message, input, "name");
    }
}

package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class AddTodoCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public AddTodoCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        String authorId = message.getAuthor().getId();
        Event event = bucketlistService.addTodo(Integer.parseInt(input[2]), new Todo(input[3]));

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN);
        embedBuilder.setTitle(String.format("Successfully added a Todo to Event %s (Id Event = %d)", event.getNameEvent(), event.getId()));
        for(Todo todo: event.getTodoitemlist()){
            embedBuilder.setDescription(String.format("User <@%s> add Todo: %d %s", authorId, todo.getId(), todo.getNameTodo()));
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

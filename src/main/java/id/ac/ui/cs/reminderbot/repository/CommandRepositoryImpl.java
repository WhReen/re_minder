package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.command.Command;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CommandRepositoryImpl implements  CommandRepository{
    static final Map<String, Command> fitur = new HashMap<>();

    public static Map<String, Command> getFitur(){
        return fitur;
    }

    @Override
    public Command getCommand(String commandName){
        return fitur.get("!" + commandName);
    }

    @Override
    public void addCommand(String commandName, Command command){
        fitur.put(commandName, command);
    }
}

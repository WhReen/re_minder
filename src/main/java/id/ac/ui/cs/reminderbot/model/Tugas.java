package id.ac.ui.cs.reminderbot.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table (name = "tugas")
@Data
@NoArgsConstructor
public class Tugas {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column
    private int id;

    @Column(nullable = false)
    private String namaTugas;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date deadline;

    @Column(nullable = false)
    private String guildID;

    @Column(columnDefinition = "boolean default false")
    private boolean checked;

    public Tugas(String namaTugas, Date deadline, String guildID){
        this.namaTugas = namaTugas;
        this.deadline = deadline;
        this.guildID = guildID;
    }
}

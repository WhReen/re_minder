package id.ac.ui.cs.reminderbot.command.kegiatan;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class DeleteKegiatanCommand implements Command {
    private KegiatanService kegiatanService;
    public Message message;

    public DeleteKegiatanCommand(KegiatanService kegiatanService){
        this.kegiatanService = kegiatanService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        //!kegiatan delete [id]
        this.message = message;
        int id = Integer.parseInt(input[2]);
        Kegiatan kegiatan = kegiatanService.deleteKegiatan(id);

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        if(kegiatan != null) {
            embedBuilder.setTitle(String.format("Kegiatan %s (Id = %d) Telah dihapus!"
                    , kegiatan.getNamaKegiatan(), kegiatan.getId()));
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Integer> {
    Todo findById(int idTodo);
}

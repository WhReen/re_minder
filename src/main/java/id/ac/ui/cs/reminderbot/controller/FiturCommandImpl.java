package id.ac.ui.cs.reminderbot.controller;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.repository.CommandRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class FiturCommandImpl implements FiturCommand{
    private CommandRepository commandRepository;

    @Autowired
    public FiturCommandImpl(CommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    public String formCommand(String[] input){
        String commandName;
        if(input.length == 1){
            commandName = input[0].substring(1);
        } else {
            commandName = input[0].substring(1) + " " + input[1];
        }
        return commandName;
    }

    @Override
    public void outputMessage(Message message, String[] input){
        try{
            Command command = commandRepository.getCommand(formCommand(input));
            command.getOutputMessage(message, input);
        } catch (Exception e){
            e.printStackTrace();
            EmbedBuilder embedBuilder = new EmbedBuilder();

            embedBuilder.setColor(Color.red);
            embedBuilder.addField("Unknown input. Use `!help` for commands.", "", false);
            message.getChannel().sendMessage(embedBuilder.build()).queue();
        }
    }
}

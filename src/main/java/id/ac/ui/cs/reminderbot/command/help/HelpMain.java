package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpMain implements Command {
    private static HelpMain uniqueInstance;

    public HelpMain() {
    }

    public static synchronized HelpMain getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new HelpMain();
        }
        return uniqueInstance;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        EmbedBuilder embedBuilder= new EmbedBuilder();
        embedBuilder.setColor(1752220);
        embedBuilder.setTitle("List help query Reminder Bot:");
        embedBuilder.addField("`!help`", "menampilkan pesan ini\n", false);
        embedBuilder.addField("`!help bucketlist`", "menampilkan daftar query pada fitur bucketlist\n", false);
        embedBuilder.addField("`!help tugas`", "menampilkan daftar query pada fitur tugas\n", false);
        embedBuilder.addField("`!help kegiatan`", "menampilkan daftar query pada fitur kegiatan\n", false);
        embedBuilder.addField("`!help find`", "menampilkan daftar query pada fitur find\n", false);
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

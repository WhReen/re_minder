package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.repository.TodoRepository;
import id.ac.ui.cs.reminderbot.repository.TugasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

@Service
public class TugasServiceImpl implements TugasService{
    @Autowired
    TugasRepository tugasRepository;

    @Autowired
    TodoRepository todoRepository;

    @Override
    public Tugas addTugas(Tugas tugas) {
        return tugasRepository.save(tugas);
    }

    @Override
    public Tugas deleteTugas(int id) {
        Tugas tugas = tugasRepository.findById(id);
        if(tugas == null) return null;
        tugasRepository.deleteById(id);
        return tugas;
    }

    @Override
    public Tugas showTugas(int id) {
        return tugasRepository.findById(id);
    }

    @Override
    public Iterable<Tugas> showAllTugas(String guildID) {
        List<Tugas> raw= tugasRepository.findAll();
        ArrayList<Tugas> listOfTasks = new ArrayList();
        for (Tugas tugas: raw) {
            if (tugas.getGuildID() != null) {
                if (tugas.getGuildID().equals(guildID)) {
                    listOfTasks.add(tugas);
                }
            }
        }
        return listOfTasks;
    }

    @Override
    public Tugas checkTugasById(int id) {
        Tugas tugas = tugasRepository.findById(id);
        if(tugas == null) return null;
        tugas.setChecked(true);
        tugasRepository.save(tugas);
        return tugas;
    }

    @Override
    public List<Tugas> findEventsByName(String name) {
        return tugasRepository.findByNamaTugasContainingIgnoreCase(name);
    }

    @Override
    public List<Tugas> findEventsByDeadline(Date deadline) {
        return tugasRepository.findByDeadlineBefore(deadline);

    }
}

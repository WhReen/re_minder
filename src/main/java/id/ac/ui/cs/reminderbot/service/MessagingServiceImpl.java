package id.ac.ui.cs.reminderbot.service;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import org.springframework.stereotype.Service;

@Service
public class MessagingServiceImpl implements MessagingService {
    @Override
    public void sendRightNow(EmbedBuilder eb, TextChannel textChannel) {
        textChannel.sendMessage(eb.build()).queue();
    }
}

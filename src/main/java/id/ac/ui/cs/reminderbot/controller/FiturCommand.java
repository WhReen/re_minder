package id.ac.ui.cs.reminderbot.controller;

import net.dv8tion.jda.api.entities.Message;

public interface FiturCommand {
    void outputMessage(Message message, String[] input);
}

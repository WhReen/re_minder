package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ShowAllTugasCommand implements Command {
    private TugasService tugasService;

    public Message message;

    public ShowAllTugasCommand(TugasService tugasService){
        this.tugasService = tugasService;
    }


    @Override
    public void getOutputMessage(Message message, String[] input) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE, dd-MMM-yyyy 'at' HH:mm");
        this.message = message;
        String guildID = message.getGuild().getId();
        ArrayList<Tugas> tugasArrayList = (ArrayList<Tugas>) tugasService.showAllTugas(guildID);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN)
                .setTitle("Showing all tasks:");
        for (Tugas tugas : tugasArrayList) {
            if (!tugas.isChecked()) {
                embedBuilder.appendDescription("- " + tugas.getNamaTugas() + " with ID: " + tugas.getId() +
                        " is due on " + dateFormat.format(tugas.getDeadline()) + "\n");
            } else {
                embedBuilder.appendDescription("~~- " + tugas.getNamaTugas() + " with ID: " + tugas.getId() +
                        " is due on " + dateFormat.format(tugas.getDeadline()) + "~~\n");
            }
        }
        message.reply(embedBuilder.build()).queue();
    }
}

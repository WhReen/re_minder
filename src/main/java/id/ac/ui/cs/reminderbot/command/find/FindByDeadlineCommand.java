package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.entities.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;

public class FindByDeadlineCommand extends FindByTemplate implements Command {

    public FindByDeadlineCommand(BucketlistService bucketlistService, TugasService tugasService,
                                 KegiatanService kegiatanService, MessagingService messagingService) {
        super(bucketlistService, tugasService, kegiatanService, messagingService);
    }

    protected MultiTypeEventList getMTEL(String deadline) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        Date deadlineDate = dateFormat.parse(deadline);
        return new MultiTypeEventList(
                new ArrayList<>(),
                tugasService.findEventsByDeadline(deadlineDate),
                // because of the Kegiatan design unfortunately this bot is UTC+7 only
                kegiatanService.findEventsByDeadline(deadlineDate.toInstant().atOffset(ZoneOffset.ofHours(7)))
        );
    }
    @Override
    public void getOutputMessage(Message message, String[] input) {
        queryTemplate(message, input, "deadline");
    }
}

package id.ac.ui.cs.reminderbot.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "events")
@Data
@NoArgsConstructor
@Getter
public class Event {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column
    private int Id;

    @Column(nullable = false)
    private String nameEvent;

    @Column(nullable = false)
    private String guildID;


    @OneToMany (mappedBy = "event")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Todo> todoitemlist = new ArrayList<>();

    public Event(String nameEvent, String guildID){
        this.Id = Id;
        this.nameEvent = nameEvent;
        this.guildID = guildID;
    }
}

package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BucketlistService {
    Event addEvent(Event event);
    Event addTodo(int idEvent, Todo todo);
    Event deleteEvent(int idEvent);
    Event showEvent(int idEvent);
    Iterable<Event> showAllEvent(String guildID);
    Todo deleteTodo(int idTodo);
    Todo checkTodoById(int id);
    List<Event> findEventsByName(String name);
}

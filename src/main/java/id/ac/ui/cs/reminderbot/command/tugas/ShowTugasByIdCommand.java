package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ShowTugasByIdCommand implements Command {
    private TugasService tugasService;

    public Message message;

    public ShowTugasByIdCommand(TugasService tugasService){
        this.tugasService = tugasService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        int id = Integer.parseInt(input[2]);
        Tugas tugas = tugasService.showTugas(id);
        long duration = calculateDuration(tugas.getDeadline());
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE, dd-MMM-yyyy 'at' HH:mm");

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN)
                .setTitle(tugas.getNamaTugas())
                .setDescription("ID: " + tugas.getId() +"\nDeadline: " + dateFormat.format(tugas.getDeadline()));

        if (calculateTimeUntil(duration, "days") > 0) {
            embedBuilder.addField("It is " + calculateTimeUntil(duration,"days") +
                    " day(s)" + " until deadline", "Good luck, have fun.", false);
        } else {
            embedBuilder.addField("It is " + calculateTimeUntil(duration, "hours") +
                    " hour(s)" + " until deadline", "Good luck, have fun", false);
        }
        if (tugas.isChecked()) {
            embedBuilder.addField("","**Task has been marked as completed**", false);
        }
        message.reply(embedBuilder.build()).queue();
    }

    public long calculateDuration(Date deadline) {
        return Math.abs(deadline.getTime() - new Date(System.currentTimeMillis()).getTime());
    }

    public long calculateTimeUntil(long duration, String unit) {
        long timeUntil = 0;

        switch (unit) {
            case "days":
                timeUntil = TimeUnit.DAYS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "hours":
                timeUntil = TimeUnit.HOURS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "minutes":
                timeUntil = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS);
                break;
            default:
                timeUntil = 0;
        }
        return timeUntil;
    }
}

package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.repository.KegiatanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class KegiatanServiceImpl implements KegiatanService{
    @Autowired
    private KegiatanRepository kegiatanRepository;

    @Override
    public Kegiatan addKegiatan(Kegiatan kegiatan) {
        return kegiatanRepository.save(kegiatan);
    }

    @Override
    public Kegiatan deleteKegiatan(int id) {
        Kegiatan kegiatan = kegiatanRepository.findById(id);
        if(kegiatan == null) return null;
        kegiatanRepository.deleteById(id);
        return kegiatan;
    }

    @Override
    public Kegiatan showKegiatan(int id) {
        return kegiatanRepository.findById(id);
    }

    @Override
    public Iterable<Kegiatan> showAllKegiatan(String ownerId){
        return kegiatanRepository.findAll();
    }

    @Override
    public List<Kegiatan> findEventsByName(String name) {
        return kegiatanRepository.findByNamaKegiatanContainingIgnoreCase(name);
    }

    @Override
    public List<Kegiatan> findEventsByDeadline(OffsetDateTime deadline) {
        return kegiatanRepository.findByDeadlineBefore(deadline);
    }
}

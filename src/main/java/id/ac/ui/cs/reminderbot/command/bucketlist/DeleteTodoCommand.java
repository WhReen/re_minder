package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class DeleteTodoCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public DeleteTodoCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        int idEvent = Integer.parseInt(input[2]);
        int idTodo = Integer.parseInt(input[3]);
        Todo todo = bucketlistService.deleteTodo(idTodo);

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        if(todo != null) {
            embedBuilder.setDescription(String.format("Todo %s at Event ID:%d successfully deleted!", todo.getNameTodo(), idEvent));
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

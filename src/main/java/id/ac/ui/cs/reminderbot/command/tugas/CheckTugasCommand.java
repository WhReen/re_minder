package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class CheckTugasCommand implements Command {

    private TugasService tugasService;

    public Message message;

    public CheckTugasCommand(TugasService tugasService){
        this.tugasService = tugasService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        this.message = message;
        String authorId = message.getAuthor().getId();
        int id = Integer.parseInt(input[2]);

        Tugas tugas = tugasService.checkTugasById(id);
        EmbedBuilder embedBuilder = new EmbedBuilder();

        if (tugas != null) {
            embedBuilder.setTitle("Task has been marked as done")
                    .setColor(Color.CYAN)
                    .setDescription("User " + "<@" + authorId + ">" +
                            " has checked task \"" + tugas.getNamaTugas() + "\".");
            message.reply(embedBuilder.build()).queue();
        }
        else {
            embedBuilder.setTitle("Failed to check task")
                    .setColor(Color.RED)
                    .setDescription("Task with id " + id + " does not exist");
            message.reply(embedBuilder.build()).queue();
        }

    }
}

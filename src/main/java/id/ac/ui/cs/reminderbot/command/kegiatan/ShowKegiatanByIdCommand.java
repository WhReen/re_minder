package id.ac.ui.cs.reminderbot.command.kegiatan;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ShowKegiatanByIdCommand implements Command {
    private KegiatanService kegiatanService;

    public Message message;

    public ShowKegiatanByIdCommand(KegiatanService kegiatanService){
        this.kegiatanService = kegiatanService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        int id = Integer.parseInt(input[2]);
        Kegiatan kegiatan = kegiatanService.showKegiatan(id);
        OffsetDateTime now = new Date(System.currentTimeMillis())
                .toInstant().atOffset(kegiatan.getStartTime().getOffset());
        long duration = Duration.between(now, kegiatan.getStartTime()).toMillis();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E, dd-MMM-yyyy 'at' HH:mm");

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN)
                .setTitle(kegiatan.getNamaKegiatan())
                .setDescription("ID: " + kegiatan.getId() +"\nStarts: " + dateFormat.format(kegiatan.getStartTime())
                + "\nEnds: " + dateFormat.format(kegiatan.getEndTime()));

        if (calculateTimeUntil(duration, "days") > 0) {
            embedBuilder.addField("It starts in " + calculateTimeUntil(duration,"days") +
                    " day(s)" , "Good luck, have fun.", false);
        } else {
            embedBuilder.addField("It starts in " + calculateTimeUntil(duration, "hours") +
                    " hour(s)" , "Good luck, have fun", false);
        }
        message.reply(embedBuilder.build()).queue();
    }

    public long calculateTimeUntil(long duration, String unit) {
        long timeUntil = 0;

        switch (unit) {
            case "days":
                timeUntil = TimeUnit.DAYS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "hours":
                timeUntil = TimeUnit.HOURS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "minutes":
                timeUntil = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS);
                break;
            default:
                timeUntil = 0;
        }
        return timeUntil;
    }
}

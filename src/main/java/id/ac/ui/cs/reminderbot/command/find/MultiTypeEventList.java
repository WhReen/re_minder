package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.model.Tugas;

import java.util.List;

public class MultiTypeEventList {
    private final List<Event>    bucketlistList;
    private final List<Tugas>    tugasList;
    private final List<Kegiatan> kegiatanList;
    public MultiTypeEventList(List<Event> bucketlistList, List<Tugas> tugasList, List<Kegiatan> kegiatanList) {
        this.bucketlistList = bucketlistList;
        this.tugasList = tugasList;
        this.kegiatanList = kegiatanList;
    }

    public List<Event> getBucketlistList() {
        return bucketlistList;
    }

    public List<Tugas> getTugasList() {
        return tugasList;
    }

    public List<Kegiatan> getKegiatanList() {
        return kegiatanList;
    }
}
package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpBucketList implements Command {
    private static HelpBucketList uniqueInstance;

    public HelpBucketList() {
    }

    public static synchronized HelpBucketList getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new HelpBucketList();
        }
        return uniqueInstance;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(1752220);
        embedBuilder.setTitle("List help query Reminder Bot for BucketList");
        embedBuilder.addField("`!bucketlist showEvent idEvent`", "Show detail bucketlist from id\n", false);
        embedBuilder.addField("`!bucketlist showAllEvent`", "Show detail of all bucketlist\n", false);
        embedBuilder.addField("`!bucketlist addTodo idEvent namaTodo`", "Add todo to specific bucketlist\n", false);
        embedBuilder.addField("`!bucketlist add namaEvent`", "Add a bucketlist event\n", false);
        embedBuilder.addField("`!bucketlist deleteEvent idEvent`", "Delete bucketlist\n", false);
        embedBuilder.addField("`!bucketlist deleteTodo idTodo`", "Delete todo in bucketlist\n", false);
        embedBuilder.addField("`!bucketlist checkTodo idTodo`", "Crossing out a todo item on a bucketlist\n", false);
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

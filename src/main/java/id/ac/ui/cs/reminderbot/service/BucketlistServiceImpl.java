package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.repository.EventRepository;
import id.ac.ui.cs.reminderbot.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BucketlistServiceImpl implements BucketlistService{
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private TodoRepository todoRepository;

    @Override
    public Event addEvent(Event event){
        return eventRepository.save(event);
    }

    @Override
    public Event addTodo(int idEvent, Todo todo){
        Event event = eventRepository.findById(idEvent);
        todo.setEvent(event);
        todoRepository.save(todo);
        return eventRepository.findById(event.getId());
    }

    @Override
    public Event deleteEvent(int idEvent){
        Event event = eventRepository.findById(idEvent);
        if(event == null) return null;
        eventRepository.deleteById(idEvent);
        return event;
    }

    @Override
    public Event showEvent(int idEvent){
        return eventRepository.findById(idEvent);
    }

    @Override
    public Iterable<Event> showAllEvent(String guildID){
        List<Event> raw= eventRepository.findAll();
        ArrayList<Event> listOfTasks = new ArrayList();
        for (Event event: raw) {
            if (event.getGuildID() != null) {
                if (event.getGuildID().equals(guildID)) {
                    listOfTasks.add(event);
                }
            }
        }
        return listOfTasks;
    }

    @Override
    public Todo deleteTodo(int idTodo){
        Todo todo = todoRepository.findById(idTodo);
        if(todo == null) return null;
        todoRepository.deleteById(idTodo);
        return todo;
    }

    @Override
    public Todo checkTodoById(int idTodo){
        Todo todo = todoRepository.findById(idTodo);
        if(todo == null) return null;
        todo.setChecked(true);
        todoRepository.save(todo);
        return todo;

    }

    @Override
    public List<Event> findEventsByName(String name) {
        return eventRepository.findByNameEventContainingIgnoreCase(name);
    }
}

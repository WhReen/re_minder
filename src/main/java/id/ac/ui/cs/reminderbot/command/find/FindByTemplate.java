package id.ac.ui.cs.reminderbot.command.find;

import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.List;

@Component
public abstract class FindByTemplate {
    protected final BucketlistService bucketlistService;
    protected final TugasService tugasService;
    protected final MessagingService messagingService;
    protected final KegiatanService kegiatanService;
    public FindByTemplate(BucketlistService bucketlistService, TugasService tugasService,
                          KegiatanService kegiatanService, MessagingService messagingService) {
        this.bucketlistService = bucketlistService;
        this.tugasService = tugasService;
        this.messagingService = messagingService;
        this.kegiatanService = kegiatanService;
    }

    public void queryTemplate(Message message, String[] input, String field) {
        String arg = input[2];
        EmbedBuilder eb = getEmbeddedMessage(field, arg);
        messagingService.sendRightNow(eb, message.getTextChannel());
    }

    protected MultiTypeEventList getMTEL(String arg) throws Exception {
        return null;
    }

    public EmbedBuilder getEmbeddedMessage(String field, String arg) {
        EmbedBuilder eb;
        try {
            MultiTypeEventList mtel = getMTEL(arg);
            List<Event> bucketlistList = mtel.getBucketlistList();
            List<Tugas> tugasList = mtel.getTugasList();
            List<Kegiatan> kegiatanList = mtel.getKegiatanList();

            if (bucketlistList == null || tugasList == null || kegiatanList == null) {
                eb = getNullListMessage();
            }
            else if ((bucketlistList.size() + tugasList.size() + kegiatanList.size()) < 1) {
                eb = getEmptyListMessage(field, arg);
            }
            else {
                int numOfEvents = (bucketlistList.size() + tugasList.size() + kegiatanList.size());
                eb = buildMessage("Events found!", Color.YELLOW, "Found " + numOfEvents + " upcoming events.");
                eb = appendListsToEmbedBuilder(eb, bucketlistList, tugasList, kegiatanList);

            }
        }
        catch (Exception e) {
            eb = buildMessage("Something went wrong!", Color.RED, e.getMessage());
        }
        return eb;
    }

    public EmbedBuilder buildMessage(String title, Color color, String description) {
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(title);
        eb.setColor(color);
        eb.setDescription(description);
        return eb;
    }

    public EmbedBuilder getNullListMessage() {
        return buildMessage("Something went wrong!", Color.RED,
                "We've outsourced our labor to Bornean orangutans and something went wrong");
    }

    public EmbedBuilder getEmptyListMessage(String field, String arg) {
        return buildMessage("No events found!", Color.RED,
                "No events found by the field: " + field + ", with value: " + arg + ".");
    }

    public EmbedBuilder appendListsToEmbedBuilder(EmbedBuilder that, List<Event> bucketlistList, List<Tugas> tugasList,
                                          List<Kegiatan> kegiatanList) {
        EmbedBuilder eb = new EmbedBuilder(that);
        for (Event e : bucketlistList) {
            eb.addField(e.getNameEvent() + " (Bucketlist: "+ e.getId() +")",
                    "Due by never",
                    false);
        }

        for (Tugas e : tugasList) {
            eb.addField(e.getNamaTugas() + " (Tugas: "+ e.getId() +")",
                    "Due by "  + e.getDeadline().toString(),
                    false);
        }

        for (Kegiatan e : kegiatanList) {
            eb.addField(e.getNamaKegiatan() + " (Kegiatan: "+ e.getId() +")",
                    "Due by "  + e.getStartTime().toString(),
                    false);
        }
        return eb;
    }
}

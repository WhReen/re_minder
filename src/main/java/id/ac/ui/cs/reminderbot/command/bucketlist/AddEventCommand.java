package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class AddEventCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;
    public String outputMsg;

    public AddEventCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        String authorId = message.getAuthor().getId();
        String guildID = message.getGuild().getId();
        Event event = bucketlistService.addEvent(new Event(input[2], guildID));

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN);

        outputMsg = String.format("User <@%s> add an event bucketlist: %s with id: %d", authorId, event.getNameEvent(), event.getId());
        embedBuilder.setTitle("Successfully added a Bucketlist\n");
        embedBuilder.setDescription(outputMsg);
        message.reply(embedBuilder.build()).queue();
    }
}

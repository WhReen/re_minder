package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.model.Tugas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TugasRepository extends JpaRepository<Tugas, Integer> {
    Tugas findById(int id);
    List<Tugas> findByNamaTugasContainingIgnoreCase(String name);

    @Query("select t from Tugas t where t.deadline <= :deadline")
    List<Tugas> findByDeadlineBefore(
            @Param("deadline") Date deadline);
}

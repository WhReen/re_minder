package id.ac.ui.cs.reminderbot;

import id.ac.ui.cs.reminderbot.controller.FiturCommand;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

@Service
public class InputEventListener extends ListenerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(InputEventListener.class);

    @Value("${prefix}")
    private String prefix;
    private String[] content;
    private Map<String, String> Command = new HashMap<String,String>();

    @Autowired
    private FiturCommand fiturCommand;

    @Autowired
    private BucketlistService bucketlistService;

    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event){
        Message message = event.getMessage();
        content = message.getContentRaw().split(" ");

        if (message.getAuthor().isBot()) return;

        if (message.getContentRaw().startsWith(prefix)){
            fiturCommand.outputMessage(message, content);
        }
    }
}

package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpKegiatan implements Command {
    private static HelpKegiatan uniqueInstance;

    public HelpKegiatan() {
    }

    public static synchronized HelpKegiatan getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new HelpKegiatan();
        }
        return uniqueInstance;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(1752220);
        embedBuilder.setTitle("List help query Re_minder Bot fitur Kegiatan");
        embedBuilder.addField("`!kegiatan add [nama] [mulai] [selesai/durasiJam] (tanggal)`", "Add new\n", false);
        embedBuilder.addField("`!kegiatan delete [id]`", "Deletes with a certain ID\n", false);
        embedBuilder.addField("`!kegiatan showAll`", "Shows all that currently available\n", false);
        embedBuilder.addField("`!kegiatan showById [id]`", "Shows that have certain ID\n", false);
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }

}
package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpEventTugas implements Command {
    private static HelpEventTugas uniqueInstance;

    public HelpEventTugas() {
    }

    public static synchronized HelpEventTugas getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new HelpEventTugas();
        }
        return uniqueInstance;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(1752220);
        embedBuilder.setTitle("List help query Re_minder Bot fitur Event Tugas");
        embedBuilder.addField("`!tugas add [NAMA] [DEADLINE(dd-MM-yyyy_HH:mm)]`", "Add a new task with a deadline\n", false);
        embedBuilder.addField("`!tugas check [id]`", "Mark task as done\n", false);
        embedBuilder.addField("`!tugas delete [ID]`", "Deletes task with a certain ID\n", false);
        embedBuilder.addField("`!tugas showAll`", "Shows all tasks currently available\n", false);
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }

}
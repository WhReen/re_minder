package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.model.Kegiatan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
public interface KegiatanRepository extends JpaRepository<Kegiatan, Integer> {
    Kegiatan findById(int id);
    List<Kegiatan> findByNamaKegiatanContainingIgnoreCase(String name);

    @Query("select k from Kegiatan k where k.startTime <= :deadline")
    List<Kegiatan> findByDeadlineBefore(
            @Param("deadline") OffsetDateTime deadline);
}

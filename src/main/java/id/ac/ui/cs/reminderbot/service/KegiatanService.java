package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Kegiatan;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;

@Component
public interface KegiatanService {
    Kegiatan addKegiatan(Kegiatan kegiatan);
    Kegiatan deleteKegiatan(int id);
    Kegiatan showKegiatan(int id);
    Iterable<Kegiatan> showAllKegiatan(String ownerId);
    List<Kegiatan> findEventsByName(String name);
    List<Kegiatan> findEventsByDeadline(OffsetDateTime deadline);
}

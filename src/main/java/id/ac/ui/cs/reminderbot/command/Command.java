package id.ac.ui.cs.reminderbot.command;

import net.dv8tion.jda.api.entities.Message;

public interface Command {
    public void getOutputMessage(Message message, String[] input);
}

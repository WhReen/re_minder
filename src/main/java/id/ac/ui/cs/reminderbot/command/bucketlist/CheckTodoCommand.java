package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class CheckTodoCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public CheckTodoCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        String authorId = message.getAuthor().getId();
        int idTodo = Integer.parseInt(input[2]);

        Todo todo = bucketlistService.checkTodoById(idTodo);
        EmbedBuilder embedBuilder = new EmbedBuilder();

        if (todo != null) {
            embedBuilder.setTitle("Todo has been marked as done")
                    .setColor(Color.CYAN)
                    .setDescription("User " + "<@" + authorId + ">" +
                            " has checked todo \"" + todo.getNameTodo() + "\".");
            message.reply(embedBuilder.build()).queue();
        }
        else {
            embedBuilder.setTitle("Failed to check todo")
                    .setColor(Color.RED)
                    .setDescription("Todo with id " + Integer.toString(idTodo) + " does not exist");
            message.reply(embedBuilder.build()).queue();
        }

    }
}

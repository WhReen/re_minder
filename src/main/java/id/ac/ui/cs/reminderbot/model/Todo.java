package id.ac.ui.cs.reminderbot.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table (name = "todo")
@Data
@NoArgsConstructor
@Getter
public class Todo {
    @Id
    @GeneratedValue
    @Column
    private int Id;

    @Column(nullable = false)
    private String nameTodo;

    @Column(columnDefinition = "boolean default false")
    private boolean checked;

    @ManyToOne()
    @JoinColumn(name = "todolist")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Event event;

    public Todo(String nameTodo){
        this.Id = Id;
        this.nameTodo = nameTodo;
    }
}

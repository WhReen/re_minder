package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class ShowAllEventCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public ShowAllEventCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        String guildID = message.getGuild().getId();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN);
        embedBuilder.setTitle("SHOW ALL EVENT");
        for(Event event : bucketlistService.showAllEvent(guildID)) {
            embedBuilder.appendDescription(String.format("\uD83D\uDCCC %s (Id Event = %d) \n", event.getNameEvent(), event.getId()));
            for(Todo todo: event.getTodoitemlist()){
                if (!todo.isChecked()) {
                    embedBuilder.appendDescription("- " + todo.getNameTodo() + " with ID: " + todo.getId() + "\n");
                } else {
                    embedBuilder.appendDescription("~~- " + todo.getNameTodo() + " with ID: " + todo.getId() + "~~\n");
                }
            }
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

package id.ac.ui.cs.reminderbot.service;

import id.ac.ui.cs.reminderbot.model.Tugas;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public interface TugasService {
    Tugas addTugas(Tugas tugas);
    Tugas deleteTugas(int id);
    Tugas showTugas(int id);
    List<Tugas> findEventsByName(String name);
    List<Tugas> findEventsByDeadline(Date deadline);
    Iterable<Tugas> showAllTugas(String guildID);
    Tugas checkTugasById(int id);
}

package id.ac.ui.cs.reminderbot.service;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.TextChannel;

public interface MessagingService {
    void sendRightNow(EmbedBuilder eb, TextChannel textChannel);
}

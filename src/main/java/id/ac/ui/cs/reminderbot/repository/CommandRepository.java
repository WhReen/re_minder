package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.command.Command;

public interface CommandRepository {
    Command getCommand(String commandName);
    void addCommand(String commandName, Command command);
}

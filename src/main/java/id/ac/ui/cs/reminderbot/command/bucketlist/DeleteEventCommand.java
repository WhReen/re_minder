package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class DeleteEventCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public DeleteEventCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        int idEvent = Integer.parseInt(input[2]);
        Event event = bucketlistService.deleteEvent(idEvent);

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.RED);
        if(event != null) {
            embedBuilder.setDescription(String.format("Event %s (Id Event = %d) Telah dihapus", event.getNameEvent(), event.getId()));
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

package id.ac.ui.cs.reminderbot.command.kegiatan;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class AddKegiatanCommand implements Command {
    private KegiatanService kegiatanService;
    public Message message;

    public AddKegiatanCommand(KegiatanService kegiatanService){
        this.kegiatanService = kegiatanService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        //!kegiatan add [nama] [mulai] [selesai/durasiJam] (tanggal)
        this.message = message;
        String ownerId = message.getGuild().getId();
        boolean isOnGuild = true;
        if (message.getGuild() == null) {
            ownerId = message.getAuthor().getId();
            isOnGuild = false;
        }
        int offset = +7; //offset WIB
        OffsetDateTime timeCreated = new Date(System.currentTimeMillis())
                .toInstant().atOffset(ZoneOffset.ofHours(offset));
        String[] start = input[3].split(":");
        int hour = Integer.parseInt(start[0]);
        int minute = Integer.parseInt(start[1]);
        int year = timeCreated.getYear();
        int month = timeCreated.getMonthValue();
        int day = timeCreated.getDayOfMonth();
        if (input.length == 6) {
            String[] date = input[5].split("-");
            if (date.length >= 2) day = Integer.parseInt(date[0]);
            if (date.length >= 3) month = Integer.parseInt(date[1]);
            if (date.length == 4) year = Integer.parseInt(date[2]);
        }
        if (hour < timeCreated.getHour()) day++;
        if (day < timeCreated.getDayOfMonth()) month++;
        if (month < timeCreated.getMonthValue()) year++;

        OffsetDateTime startTime = OffsetDateTime.of(year, month, day,
                hour, minute, 0, 0,
                ZoneOffset.ofHours(offset));
        OffsetDateTime endTime;
        if (input[4].contains(":")) {
            String[] end = input[4].split(":");
            endTime = startTime.plusHours(Integer.parseInt(end[0])-startTime.getHour())
                    .plusMinutes(Integer.parseInt(end[1])-startTime.getMinute());
        } else {
            endTime = startTime.plusHours(Integer.parseInt(input[4]));
        }

        Kegiatan kegiatan = kegiatanService.addKegiatan(new Kegiatan(input[2], startTime, endTime, ownerId, isOnGuild));

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E, dd-MMM-yyyy 'at' HH:mm");
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN);
        embedBuilder.setTitle("Successfully added a Kegiatan");
        embedBuilder.addField("", String.format("Id: %d \nName: %s \nStart Time: %s \nEnd Time: %s"
                , kegiatan.getId(), kegiatan.getNamaKegiatan()
                , dateFormat.format(kegiatan.getStartTime()), dateFormat.format(kegiatan.getEndTime())), false);
        message.reply(embedBuilder.build()).queue();

        OffsetDateTime now = new Date(System.currentTimeMillis())
                .toInstant().atOffset(kegiatan.getStartTime().getOffset());
        long duration = Duration.between(now, kegiatan.getStartTime()).toMillis();
        long ends = Duration.between(kegiatan.getStartTime(), kegiatan.getEndTime()).toMillis();
        long daysUntil = calculateTimeUntil(duration, "days");
        long hoursUntil = calculateTimeUntil(duration, "hours");
        long minutesUntil = calculateTimeUntil(duration, "minutes");
        String authorID = message.getAuthor().getId();
        if (daysUntil >= 1) {
            message.reply("Hey <@" + authorID + "> " + input[2] +
                    " starts in less than a day!").queueAfter(daysUntil - 1, TimeUnit.DAYS);
        }
        if (hoursUntil > 1) {
            message.reply("Hey <@" + authorID + "> " + input[2] +
                    " starts in an hour!").queueAfter(hoursUntil - 1, TimeUnit.HOURS);
        }
        if (minutesUntil > 30) {
            message.reply("Hey <@" + authorID + "> " + input[2] +
                    " starts in 30 minutes!").queueAfter(minutesUntil - 30, TimeUnit.MINUTES);
        }
        if (minutesUntil < 30) {
        message.reply("Hey <@" + authorID + "> " + input[2] +
                " starts in "+ minutesUntil + " minute(s)!").queue();
        }
        message.reply("Hey <@" + authorID + "> " + input[2] +
                " **STARTS NOW!**").queueAfter(duration, TimeUnit.MILLISECONDS);

        message.getChannel().sendMessage("Hey <@" + authorID + "> " + input[2] +
                " ends now.. yay!").queueAfter(duration+ends, TimeUnit.MILLISECONDS);
    }

    public long calculateTimeUntil(long duration, String unit) {
        long timeUntil = 0;

        switch (unit) {
            case "days":
                timeUntil = TimeUnit.DAYS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "hours":
                timeUntil = TimeUnit.HOURS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "minutes":
                timeUntil = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS);
                break;
            default:
                timeUntil = 0;
        }
        return timeUntil;
    }
}

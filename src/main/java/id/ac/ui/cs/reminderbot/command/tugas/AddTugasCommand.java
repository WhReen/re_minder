package id.ac.ui.cs.reminderbot.command.tugas;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Tugas;
import id.ac.ui.cs.reminderbot.service.TugasService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class AddTugasCommand implements Command {

    private TugasService tugasService;

    public Message message;

    public AddTugasCommand(TugasService tugasService){
        this.tugasService = tugasService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input) {
        this.message = message;
        String guildID = message.getGuild().getId();
        String authorID = message.getAuthor().getId();
        Date now = new Date(System.currentTimeMillis());

        String namaTugas = input[2];
        String deadlineString = input[3];

        Date deadline = parseStringToDate(deadlineString);
        long duration = calculateDuration(deadline);

        Tugas tugas = tugasService.addTugas(new Tugas(namaTugas, deadline, guildID));

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN)
                .setTitle("Adding task")
                .setDescription("It is currently " + parseDateToString(now) +
                        "\n" + "<@" + authorID + ">" + " is adding task \"" + namaTugas + "\" due on " +
                        deadline);

        if (calculateTimeUntil(duration, "days") > 0) {
            embedBuilder.addField("It is " + calculateTimeUntil(duration,"days") +
                            " day(s)" + " until deadline", "Good luck, have fun.", false);
        } else {
            embedBuilder.addField("It is " + calculateTimeUntil(duration, "hours") +
                            " hour(s)" + " until deadline", "Good luck, have fun", false);
        }
        embedBuilder.addField("", String.format("Id: %d", tugas.getId()), false);
        message.reply(embedBuilder.build()).queue();


        remind(duration,authorID,namaTugas);

    }
    public void remind(long duration, String authorID, String namaTugas){
        long daysUntil = calculateTimeUntil(duration, "days");
        long hoursUntil = calculateTimeUntil(duration, "hours");
        long minutesUntil = calculateTimeUntil(duration, "minutes");
        if (daysUntil >= 1) {
            message.getChannel().sendMessage ("Hey <@" + authorID + "> " + namaTugas +
                    " is due in less than a day!").queueAfter(daysUntil - 1, TimeUnit.DAYS);
        }
        if (hoursUntil > 1) {
            message.getChannel().sendMessage("Hey <@" + authorID + "> " + namaTugas +
                    " is due in an hour!").queueAfter(hoursUntil - 1, TimeUnit.HOURS);
        }
        message.getChannel().sendMessage("Hey <@" + authorID + "> " + namaTugas +
                " is due in 30 minutes!").queueAfter(minutesUntil - 30, TimeUnit.MINUTES);

        message.getChannel().sendMessage("Hey <@" + authorID + "> " + namaTugas +
                " is due in 15 minutes!").queueAfter(minutesUntil - 15, TimeUnit.MINUTES);

        message.reply("Hey <@" + authorID + "> " + namaTugas +
                " is due in 5 minutes!").queueAfter(minutesUntil - 5, TimeUnit.MINUTES);
    }

    public Date parseStringToDate(String deadlineString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm");
        Date deadline = null;

        try {
            deadline = dateFormat.parse(deadlineString);
        } catch (ParseException e) {
            EmbedBuilder eb = new EmbedBuilder();
            eb.setTitle("Incorrect date format!")
                    .setDescription("Correct usage: !tugas add [NAMA] [dd-MM-yyyy_HH:mm]")
                    .setColor(Color.red);
            message.reply(eb.build()).queue();
            e.printStackTrace();
        }

        return deadline;
    }

    public String parseDateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        return dateFormat.format(date);
    }

    public long calculateDuration(Date deadline) {
        return Math.abs(deadline.getTime() - new Date(System.currentTimeMillis()).getTime());
    }

    public long calculateTimeUntil(long duration, String unit) {
        long timeUntil = 0;

        switch (unit) {
            case "days":
                timeUntil = TimeUnit.DAYS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "hours":
                timeUntil = TimeUnit.HOURS.convert(duration, TimeUnit.MILLISECONDS);
                break;
            case "minutes":
                timeUntil = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS);
                break;
            default:
                timeUntil = 0;
        }

        return timeUntil;
    }
}

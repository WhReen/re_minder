package id.ac.ui.cs.reminderbot.command;

import id.ac.ui.cs.reminderbot.command.bucketlist.*;
import id.ac.ui.cs.reminderbot.command.find.FindByDeadlineCommand;
import id.ac.ui.cs.reminderbot.command.find.FindByNameCommand;
import id.ac.ui.cs.reminderbot.command.help.HelpBucketList;
import id.ac.ui.cs.reminderbot.command.help.HelpEventTugas;
import id.ac.ui.cs.reminderbot.command.help.HelpKegiatan;
import id.ac.ui.cs.reminderbot.command.help.HelpFind;
import id.ac.ui.cs.reminderbot.command.help.HelpMain;
import id.ac.ui.cs.reminderbot.command.kegiatan.AddKegiatanCommand;
import id.ac.ui.cs.reminderbot.command.kegiatan.DeleteKegiatanCommand;
import id.ac.ui.cs.reminderbot.command.kegiatan.ShowAllKegiatanCommand;
import id.ac.ui.cs.reminderbot.command.kegiatan.ShowKegiatanByIdCommand;
import id.ac.ui.cs.reminderbot.command.tugas.*;
import id.ac.ui.cs.reminderbot.repository.CommandRepository;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import id.ac.ui.cs.reminderbot.service.MessagingService;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import id.ac.ui.cs.reminderbot.service.TugasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;

@Component
public class InitCommand {
    private CommandRepository commandRepository;
    private BucketlistService bucketlistService;
    private TugasService tugasService;
    private KegiatanService kegiatanService;
    private MessagingService messagingService;

    @Autowired
    public InitCommand(CommandRepository commandRepository, BucketlistService bucketlistService
            , KegiatanService kegiatanService, TugasService tugasService,
                       MessagingService messagingService) {
        this.commandRepository = commandRepository;
        this.bucketlistService = bucketlistService;
        this.kegiatanService = kegiatanService;
        this.tugasService = tugasService;
        this.messagingService = messagingService;
    }

    @PostConstruct
    public void init() throws LoginException {
        commandRepository.addCommand("!find bydate", new FindByDeadlineCommand(
                bucketlistService, tugasService, kegiatanService, messagingService));
        commandRepository.addCommand("!find byname", new FindByNameCommand(
                bucketlistService, tugasService, kegiatanService, messagingService));
        commandRepository.addCommand("!tugas add", new AddTugasCommand(tugasService));
        commandRepository.addCommand("!tugas delete", new DeleteTugasCommand(tugasService));
        commandRepository.addCommand("!tugas showall", new ShowAllTugasCommand(tugasService));
        commandRepository.addCommand("!tugas show", new ShowTugasByIdCommand(tugasService));
        commandRepository.addCommand("!tugas check", new CheckTugasCommand(tugasService));
        commandRepository.addCommand("!help find", new HelpFind());
        commandRepository.addCommand("!help bucketlist", new HelpBucketList());
        commandRepository.addCommand("!help tugas", new HelpEventTugas());
        commandRepository.addCommand("!help", new HelpMain());
        commandRepository.addCommand("!help kegiatan", new HelpKegiatan());
        commandRepository.addCommand("!bucketlist add", new AddEventCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist addTodo", new AddTodoCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist showEvent", new ShowEventCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist showAllEvent", new ShowAllEventCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist deleteEvent", new DeleteEventCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist deleteTodo", new DeleteTodoCommand(bucketlistService));
        commandRepository.addCommand("!bucketlist checkTodo", new CheckTodoCommand(bucketlistService));
        commandRepository.addCommand("!kegiatan add", new AddKegiatanCommand(kegiatanService));
        commandRepository.addCommand("!kegiatan delete", new DeleteKegiatanCommand(kegiatanService));
        commandRepository.addCommand("!kegiatan showAll", new ShowAllKegiatanCommand(kegiatanService));
        commandRepository.addCommand("!kegiatan showById", new ShowKegiatanByIdCommand(kegiatanService));
    }
}

package id.ac.ui.cs.reminderbot.repository;

import id.ac.ui.cs.reminderbot.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    Event findById(int Id);
    List<Event> findByNameEventContainingIgnoreCase(String name);
}

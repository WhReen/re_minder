package id.ac.ui.cs.reminderbot.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table (name = "kegiatans")
@Data
@NoArgsConstructor
@Getter
public class Kegiatan {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column
    private int id;

    @Column(nullable = false)
    private String namaKegiatan;

    @Column(nullable = false)
    private OffsetDateTime startTime;

    @Column(nullable = false)
    private OffsetDateTime endTime;

    @Column(nullable = false)
    private String ownerId;

    @Column(nullable = false)
    private boolean isOnServer;

    public Kegiatan(String namaKegiatan, OffsetDateTime startTime, OffsetDateTime endTime, String ownerId
            , boolean isOnServer){
        this.id = id;
        this.namaKegiatan = namaKegiatan;
        this.startTime = startTime;
        this.endTime = endTime;
        this.ownerId = ownerId;
        this.isOnServer = isOnServer;
    }
}

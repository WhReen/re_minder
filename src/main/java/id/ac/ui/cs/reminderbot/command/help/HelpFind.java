package id.ac.ui.cs.reminderbot.command.help;

import id.ac.ui.cs.reminderbot.command.Command;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class HelpFind implements Command {
    private static HelpFind uniqueInstance;

    public HelpFind() {
    }

    public static synchronized HelpFind getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new HelpFind();
        }
        return uniqueInstance;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(1752220);
        embedBuilder.setTitle("List help query Re_minder Bot fitur find");
        embedBuilder.addField("`!find byname [NAMA (String)]`",
                "Menampilkan event yang namanya mengandung string NAMA, case insensitive\n", false);
        embedBuilder.addField("`!find bydate [DATE (dd-MM-yyyy_HH:mm)]`",
                "Menampilkan event yang deadlinenya sebelum DATE\n", false);
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

package id.ac.ui.cs.reminderbot.command.bucketlist;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Event;
import id.ac.ui.cs.reminderbot.model.Todo;
import id.ac.ui.cs.reminderbot.service.BucketlistService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class ShowEventCommand implements Command {
    private BucketlistService bucketlistService;
    public Message message;

    public ShowEventCommand(BucketlistService bucketlistService){
        this.bucketlistService = bucketlistService;
    }

    @Override
    public void getOutputMessage(Message message, String[] input){
        this.message = message;
        int idEvent = Integer.parseInt(input[2]);
        Event event = bucketlistService.showEvent(idEvent);

        EmbedBuilder embedBuilder = new EmbedBuilder();
        String valueField = "";
        embedBuilder.setColor(Color.CYAN);
        if(event != null) {
            embedBuilder.appendDescription(String.format("Event %s (Id Event = %d)\n", event.getNameEvent(), event.getId()));
            embedBuilder.appendDescription("Todo item: \n");
            for(Todo todo: event.getTodoitemlist()){
                if (!todo.isChecked()) {
                    embedBuilder.appendDescription("- " + todo.getNameTodo() + " with ID: " + todo.getId());
                } else {
                    embedBuilder.appendDescription("~~- " + todo.getNameTodo() + " with ID: " + todo.getId() + "~~\n");
                }
            }
        }
        message.getChannel().sendMessage(embedBuilder.build()).queue();
    }
}

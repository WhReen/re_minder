package id.ac.ui.cs.reminderbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.security.auth.login.LoginException;

@SpringBootApplication
@ComponentScan({"id.ac.ui.cs.reminderbot", "id.ac.ui.cs.reminderbot.service", "id.ac.ui.cs.reminderbot.service"})
@EnableJpaRepositories("id.ac.ui.cs.reminderbot.repository")
public class ReminderbotApplication {

    public static void main(String[] args) throws LoginException {
        SpringApplication.run(ReminderbotApplication.class, args);
    }

}

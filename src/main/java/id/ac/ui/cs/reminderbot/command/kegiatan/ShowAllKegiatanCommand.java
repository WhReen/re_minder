package id.ac.ui.cs.reminderbot.command.kegiatan;

import id.ac.ui.cs.reminderbot.command.Command;
import id.ac.ui.cs.reminderbot.model.Kegiatan;
import id.ac.ui.cs.reminderbot.service.KegiatanService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ShowAllKegiatanCommand implements Command {
    private KegiatanService kegiatanService;

    public Message message;

    public ShowAllKegiatanCommand(KegiatanService kegiatanService){
        this.kegiatanService = kegiatanService;
    }


    @Override
    public void getOutputMessage(Message message, String[] input) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("EEEEE, dd-MMM-yyyy 'at' HH:mm");
        this.message = message;
        String ownerId = message.getAuthor().getId();
        if (message.isFromGuild()) {
            ownerId = message.getGuild().getId();
        }
        ArrayList<Kegiatan> kegiatanArrayList = (ArrayList<Kegiatan>) kegiatanService.showAllKegiatan(ownerId);
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.CYAN)
                .setTitle("Showing all tasks:");
        for (Kegiatan kegiatan : kegiatanArrayList) {
            embedBuilder.appendDescription("- " + kegiatan.getNamaKegiatan() + " with ID: " + kegiatan.getId()
                    + " starts on " + kegiatan.getStartTime().format(dateFormat) + " to "
                    + kegiatan.getEndTime().format(dateFormat) + "\n");
        }
        message.reply(embedBuilder.build()).queue();
    }
}

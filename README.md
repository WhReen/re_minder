[![pipeline status](https://gitlab.com/WhReen/re_minder/badges/master/pipeline.svg)](https://gitlab.com/WhReen/re_minder/-/commits/master)

[![coverage report](https://gitlab.com/WhReen/re_minder/badges/master/coverage.svg)](https://gitlab.com/WhReen/re_minder/-/commits/master)


Tugas Kelompok Advanced Programming 2021 Kelompok B-13

Anggota:
1. Arllivandhya Dani (1906398521)
2. Beltsazar Anugrah Sotardodo (1906398351)
3. Mirsa Salsabila (1906350875)
4. Ryanda Mahdy Ananda (1906351064)

List help query Re_minder Bot: <br>
- `!help`
menampilkan query yang bisa dilakukan
- `!help bucketlist`
menampilkan daftar query pada fitur bucketlist
- `!help tugas`
menampilkan daftar query pada fitur tugas

List bucketlist query Re_minder Bot fitur BucketList
- `!bucketlist showEvent idEvent`
Show detail bucketlist from id
- `!bucketlist showAllEvent`
  Show detail of all bucketlist
- `!bucketlist addTodo idEvent namaTodo`
  Add todo to specific bucketlist
- `!bucketlist add namaEvent`
  Add a bucketlist event
- `!bucketlist deleteEvent idEvent`
  Delete bucketlist
- `!bucketlist deleteTodo idTodo`
  Delete todo in bucketlist
- `!bucketlist checkTodo idTodo`
  Crossing out a todo item on a bucketlist

List help query Re_minder Bot fitur Event Tugas
- `!tugas add [NAMA] [DEADLINE(dd-MM-yyyy_HH:mm)]`
Menambahkan sebuah tugas baru dengan deadline
- `!tugas delete [ID]`
Menghapus tugas dengan ID tertentu
- `!tugas showAll`
Menampilkan semua data tugas yang sudah ditambahkan
  
List help query re:minder bot untuk fitur find
- `!find byname [NAMA (String)]`
Menampilkan event yang namanya mengandung string NAMA, case insensitive
- `!find bydate [DATE (dd-MM-yyyy_HH:mm)]`
Menampilkan event yang deadlinenya sebelum DATE
